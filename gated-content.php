<?php

/*
Plugin Name: Gated Content
Plugin URI:
Description:
Version: 1.0.0
Author: Lewis Dorigo
Author URI: https://dorigo.co
Copyright: Lewis Dorigo
*/

namespace Dorigo;

if(file_exists(__DIR__.'/vendor/autoload.php')) {
    require_once(__DIR__.'/vendor/autoload.php');
}

class GatedContent {
    private static $instance;

    private $settings;

    public $dir;
    public $file;

    private $methodClass;

    private $customFields = [];

    public $metaId = '_drgo_gated_content';

    public static $cookieName  = 'drgo_gated_access';

    public $defaultMethod = 'mailchimp';

    private $methods = [
        'mailchimp' => [
            'name'  => 'MailChimp',
            'class' => '\Dorigo\GatedContent\MailChimp',
        ],
        'campaign-monitor' => [
            'name'  => 'Campaign Monitor',
            'class' => '\Dorigo\GatedContent\CampaignMonitor',
        ],
    ];

    private function __construct() {
        $this->file = __FILE__;
        $this->dir = dirname(__FILE__);

        require_once($this->dir.'/inc/settings.php');

        require_once($this->dir.'/inc/base.class.php');
        require_once($this->dir.'/inc/mailchimp.class.php');
        require_once($this->dir.'/inc/campaign-monitor.class.php');

        $this->getMethodClass();

        $this->settings = \Dorigo\GatedContent\Settings::instance($this);

        $this->addFilters();
    }

    public static function instance() {
        if(self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }





    public function addFilters() {
        add_filter('acf/location/rule_types', [$this, 'acfRuleTypes']);
        add_filter('acf/location/rule_values/post_visibility', [$this, 'acfRuleValues']);
        add_filter('acf/location/screen', [$this, 'acfVisibilityScreen']);
        add_filter('acf/location/rule_match/post_visibility', [$this, 'acfRulesMatch'], 10, 3);

        add_filter('acf/load_field/key=field_5a534736d7daf', [$this, 'gatedContentFieldOptions']);



        add_action('admin_enqueue_scripts', [$this, 'enqueueAdminScripts']);
        add_action('wp_enqueue_scripts', [$this, 'enqueueScripts']);



        add_filter('wp_insert_post_data', [$this, 'insertPostData'], 10, 2);
        add_action('wp_insert_post', [$this, 'setPostVisibilityMeta'], 10, 3);

        add_filter('display_post_states', [$this, 'postStates'], 10, 2);
        add_action('admin_notices', [$this, 'notice'], 10);

        add_filter('Dorigo\Gated\Fields', [$this, 'gatedContentField']);


        if(function_exists('acf_add_local_field_group')) {
            add_action('after_setup_theme', [$this, 'addCustomFields'], 10);
        }

        add_filter('wp', [$this, 'preventCache'], 0);
    }





    public function preventCache() {
        if(is_singular() || is_page() || is_single()) {

            if($this->isGated()) {
                define('DONOTCACHEPAGE', true);
            }
        }
    }



    public function acfRuleTypes($rules) {
        $post_rules = $rules[__("Post",'acf')];

        $offset = 2;

        $post_rules = array_slice($post_rules, 0, $offset, true) +
                      ['post_visibility' => 'Post Visibility'] +
                      array_slice($post_rules, $offset, NULL, true);


        $rules[__("Post",'acf')] = $post_rules;

        return $rules;
    }

    public function acfRuleValues($choices) {
        $choices = [
            'public'   => 'Public',
            'password' => 'Password Protected',
            'private'  => 'Private',
            'gated'    => 'Gated Content'
        ];

        return apply_filters('Dorigo\GatedContent\ACF\Values', $choices);
    }

    public function acfVisibilityScreen($args) {
    	if(!$args['post_id'] || isset($args['post_visibility'])) return $args;

        $post_data = get_post($args['post_id']);

        $visibility = 'public';

        if(!empty($post_data->post_password)) {
            $visibility = 'password';
        } else if($post_data->post_status === 'private') {
            $visibility = 'private';
        } else if(get_post_meta($post_data->ID, $this->metaId, true)) {
            $visibility = 'gated';
        }

        $args['post_visibility'] = $visibility;

        return $args;
    }

    public function acfRulesMatch($match, $rule, $options) {
    	if(!$options['post_id']) return false;

        if($rule['operator'] == '==') {
            return $options['post_visibility'] == $rule['value'];
        } else {
            return $options['post_visibility'] != $rule['value'];
        }
    }

    public function setPostVisibilityMeta($post_id, $post, $update) {
        $visibility = isset($_POST['visibility']) ? $_POST['visibility'] : 'public';

        if($visibility === 'gated') {
            update_post_meta($post_id, $this->metaId, true);
        } else {
            delete_post_meta($post_id, $this->metaId);
        }

        return false;
    }

    public function insertPostData($data, $postarr) {
        if(isset($postarr['visibility']) && $postarr['visibility'] === 'gated') {
            $data['post_password'] = '';
        }

        return $data;
    }

    public function postStates($post_states, $post) {
        $gated = (bool) get_post_meta($post->ID, $this->metaId, true);

        if($gated) $post_states['gated'] = 'Gated Content';

        return $post_states;
    }

    public function enqueueAdminScripts() {
        global $wp_scripts;


        if(get_current_screen()->base === 'post' && isset($_GET['post'])) {

            wp_enqueue_script('gated_acf', plugins_url('js/acf.js',__FILE__), ['jquery', 'acf-input'], '1.0.0', true);
            wp_enqueue_script('gated_visibility', plugins_url('js/visibility.js',__FILE__), ['jquery', 'post'], '1.0.0', true);

            $wp_scripts->localize('gated_visibility', 'postGated', [
                'isGated' => $this->isGated()
            ]);
        }
    }

    public function enqueueScripts() {
        wp_enqueue_script('gated_main', plugins_url('js/gated.js',__FILE__), ['jquery'], false, true);
    }

    public function notice() {
        if(is_admin() && get_current_screen()->base === 'post' && isset($_GET['post'])) {
            $visibility = get_post_meta($_GET['post'], $this->metaId, true);

            if(!$visibility) return; ?>
            <div class="notice notice-warning">
                <?= wpautop(__('This post is a gated content page. Unless otherwise stated, content you enter will not be visible until the user enters their email address.')); ?>
            </div>
            <?php

        }
    }

    private function getCustomFields() {
        if(!$this->customFields) {

            $fields = apply_filters('Brave\ACF\Content', []);

            $this->customFields = array_map([$this, 'parseFields'], $fields);

            unset($fields);
        }

        return apply_filters('Dorigo\Gated\Fields', $this->customFields);
    }

    private function parseFields($field) {

        $field['key'] = $field['key'].$this->metaId;

        if(isset($field['sub_fields'])) {
            $field['sub_fields'] = array_map([$this, 'parseFields'], $field['sub_fields']);
        }

        return $field;
    }

    public function gatedContentFieldOptions($field) {
        if(!is_admin()) { return $field; }

        $lists = $this->getMethodClass()->lists();
        $default = get_option('drgo_gated_list');

        array_walk($lists, function(&$name, $id) use($default) {
            $name = $name.($id===$default?' (default)':'');
        });

        $field['choices'] = $lists;
        $field['default_value'] = [$default];

        return $field;
    }

    public function gatedContentField($fields) {
        $fields[] = apply_filters('Dorigo\Gated\Field', [
    		'key' => '5a53471e84bd1',
    		'name' => 'gated_content',
    		'label' => 'Gated Content',
    		'display' => 'block',
    		'sub_fields' => array (
        		array (
        			'key' => 'field_5a534736d7daf',
        			'label' => 'List',
        			'name' => 'list',
        			'type' => 'select',
        			'instructions' => 'Which list would you like the user to sign up to?',
        			'required' => 1,
        			'conditional_logic' => 0,
        			'wrapper' => array (
        				'width' => '',
        				'class' => '',
        				'id' => '',
        			),
        			'choices' => array (
            			'default' => 'Default List',
        			),
        			'default_value' => array (
        			),
        			'allow_null' => 0,
        			'multiple' => 0,
        			'ui' => 0,
        			'ajax' => 0,
        			'placeholder' => '',
        			'disabled' => 0,
        			'readonly' => 0,
        		),
        		array (
        			'key' => 'field_5a53a55fe64de',
        			'label' => 'Privacy Policy',
        			'name' => 'privacy_policy',
        			'type' => 'page_link',
        			'instructions' => '',
        			'required' => 1,
        			'conditional_logic' => 0,
        			'wrapper' => array (
        				'width' => '',
        				'class' => '',
        				'id' => '',
        			),
        			'post_type' => array (
        				0 => 'page',
        			),
        			'taxonomy' => array (
        			),
        			'allow_null' => 0,
        			'multiple' => 0,
        		),
    		),
    		'min' => 1,
    		'max' => 1,
        ]);

        return $fields;
    }


    public function addCustomFields() {
        $fields = $this->getCustomFields();

        acf_add_local_field_group(array (
        	'key' => 'group'.$this->metaId,
        	'title' => 'Gated Content',
        	'fields' => array (
        		array (
        			'key' => 'field_'.$this->metaId,
        			'label' => 'Gated Content',
        			'name' => 'gated_content',
        			'type' => 'flexible_content',
        			'instructions' => 'This content will only display <em>before</em> the user has entered their email address. Add a <strong>Gated Content</strong> section to show the email form.',
        			'required' => 0,
        			'conditional_logic' => 0,
        			'wrapper' => array (
        				'width' => '',
        				'class' => '',
        				'id' => '',
        			),
        			'button_label' => 'Add Section',
        			'min' => '',
        			'max' => '',
        			'layouts' => $fields,
        		),
        	),
        	'location' => array (
        		array (
        			array (
        				'param' => 'post_type',
        				'operator' => '==',
        				'value' => 'page',
        			),
        			array (
        				'param' => 'post_visibility',
        				'operator' => '==',
        				'value' => 'gated',
        			),
        		),
        	),
        	'menu_order' => 0,
        	'position' => 'normal',
        	'style' => 'default',
        	'label_placement' => 'top',
        	'instruction_placement' => 'label',
        	'hide_on_screen' => '',
        	'active' => 1,
        	'description' => '',
        ));
    }


    public static function formAction() {
        return admin_url('admin-ajax.php');
    }

    public function formFields() {
        $this->getMethodClass();

        return $this->methodClass ? $this->methodClass::formFields() : null;
    }

    public function isGated($post_id = null) {
        $post = get_post($post_id);

        return (bool) get_post_meta($post->ID, $this->metaId, true);
    }

    public function requiresGate($post_id = null) {
        if((!is_single() && !is_page() && !is_singular()) || !$this->isGated($post_id)) { return false; }

        if(defined('WP_ENV') && WP_ENV !== 'development' && is_user_logged_in()) { return false; }

        $post = get_post($post_id);
        $cookie = isset($_COOKIE[self::$cookieName]) ? json_decode(stripslashes($_COOKIE[self::$cookieName]), true) : false;

        return $cookie ? !array_key_exists($post->ID, $cookie) : true;
    }

    public function filterMethods() {
        $this->methods = array_filter($this->methods, function($method) {
            return class_exists($method['class']);
        });
    }

    public function getMethods() {
        return $this->methods;
    }

    public function getMethod($method = null, $field = null) {
        $method = $method ?: get_option('drgo_gated_method');

        if(!$method) return false;

        $current = array_key_exists($method, $this->methods) ? array_merge(['id'=>$method], $this->methods[$method]) : false;

        if($field) {
            return array_key_exists($field, $current) ? $current[$field] : false;
        } else {
            return $current;
        }
    }

    public function getMethodClass() {

        if($this->methodClass === null) {
            $method = $this->getMethod();

            if($method) {
                if(isset($method['key']) && !get_option($method['key'])) return false;

                $this->methodClass = $method['class']::instance();
            }

        }

        return $this->methodClass;
    }


}

global $gatedContent;

$gatedContent = GatedContent::instance();