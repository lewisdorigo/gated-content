<?php

namespace Dorigo\GatedContent;

class Settings {
    private static $instance;
    private $app;

    private $settings = [
        [
            'name' => 'drgo_gated_method',
            'label' => 'Default Method',
        ],

        [
            'name' => 'drgo_gated_api_key',
            'label' => 'API Key',
            'methods' => [
                'mailchimp', 'campaign-monitor',
            ],
        ],

        [
            'name' => 'drgo_gated_client',
            'label' => 'Client',
            'methods' => [
                'campaign-monitor',
            ],
        ],

        [
            'name' => 'drgo_gated_list',
            'label' => 'Default List',
            'methods' => [
                'mailchimp', 'campaign-monitor',
            ],
        ],

    ];

    private $menuSlug = 'drgo_gated';

    private function __construct($app) {
        $this->app = $app;

        add_action('admin_menu', [$this, 'menu']);
        add_action('admin_init', [$this, 'registerSettings']);

        add_action('admin_enqueue_scripts', [$this, 'enqueueAdminScripts']);
    }

    public static function instance($app) {
        if(self::$instance === null) {
            self::$instance = new self($app);
        }

        return self::$instance;
    }

    public function getSettings() {
        return apply_filters('Dorigo/Gated/Settings', $this->settings);
    }

    public function menu() {
        add_options_page('Gated Content Settings','Gated Content','manage_options', $this->menuSlug, [$this, 'settingsOutput']);
    }

    public function registerSettings() {
        foreach($this->getSettings() as $setting) {
            register_setting($this->menuSlug, $setting['name']);
        }
    }

    public function enqueueAdminScripts() {
        if(get_current_screen()->base === 'settings_page_'.$this->menuSlug) {
            wp_enqueue_script('gated_settings', plugins_url('js/settings.js',$this->app->file), ['jquery'], '1.0.0', true);
        }
    }

    public function settingsOutput() {
        echo '<div class="wrap">';
            echo '<h2>MailChimp Settings</h2>';

            echo '<style>';
                echo '#gated-content-form.form-loading { position: relative; }';
                echo '#gated-content-form.form-loading::after { content: ""; display:block;position:absolute;top:0;left:0;right:0;bottom:0;width:100%;height:100%;background:rgba(241, 241, 241, 0.82); z-index:10; }';
                echo '#gated-content-form.form-loading::before { content: "Loading…"; font-weight:bold; font-size: 1.2em; top:50%; left: 50%; transform: translate(-50%,-50%);z-index:11;position: absolute; }';
            echo '</style>';

            echo '<form action="options.php" method="post" id="gated-content-form">';
                settings_fields($this->menuSlug);

                $currentMethod = $this->app->getMethod();
                $api = $this->app->getMethodClass();

                echo '<table class="form-table">';
                    echo '<tbody>';

                    foreach($this->getSettings() as $setting) {
                        $hidden = isset($setting['methods']) && !in_array($currentMethod['id'], $setting['methods']);

                        echo '<tr '.(isset($setting['methods']) ? 'data-methods="'.implode(' ', $setting['methods']).'"' : '').($hidden?' style="display:none;"':'').'>';
                            echo '<th scope="row">';
                                echo '<label for="'.$setting['name'].'">'.$setting['label'].'</label>';
                            echo '</th>';

                            echo '<td>';
                                $value = get_option($setting['name']);

                                switch($setting['name']) {
                                    case 'drgo_gated_method':

                                        echo '<select name="'.$setting['name'].'" id="'.$setting['name'].'">';
                                            echo '<option value="" disabled'.(!$value?' selected':'').'>Please Select a Method</option>';

                                            foreach($this->app->getMethods() as $id =>$method) {
                                                echo '<option value="'.$id.'"'.($id === $value ? ' selected' :'').'>&nbsp;&nbsp;'.$method['name'].'</option>';
                                            }

                                        echo '</select>';
                                        break;

                                    case 'drgo_gated_client':

                                        echo '<select name="'.$setting['name'].'" id="'.$setting['name'].'">';
                                            echo '<option value="" disabled'.(!$value?' selected':'').'>Please Select a Client</option>';

                                            if($api && method_exists($api, 'clients')) {
                                                foreach($api->clients() as $id => $name) {
                                                    echo '<option value="'.$id.'"'.($id === $value ? ' selected' :'').'>&nbsp;&nbsp;'.$name.'</option>';
                                                }
                                            }

                                        echo '</select>';
                                        break;

                                    case 'drgo_gated_list':

                                        echo '<select name="'.$setting['name'].'" id="'.$setting['name'].'">';
                                            echo '<option value="" disabled'.(!$value?' selected':'').'>Please Select a List</option>';

                                            if($api && method_exists($api, 'lists')) {
                                                foreach($api->lists() as $id => $name) {
                                                    echo '<option value="'.$id.'"'.($id === $value ? ' selected' :'').'>&nbsp;&nbsp;'.$name.'</option>';
                                                }
                                            }

                                        echo '</select>';
                                        break;

                                    default:
                                        echo '<input type="text" name="'.$setting['name'].'" id="'.$setting['name'].'" value="'.($value ?: (isset($setting['default']) ? $setting['default'] : '')).'" class="regular-text ltr">';
                                        break;
                                }

                                if($setting['name'] === 'drgo_gated_api_key') {
                                    echo '<div data-methods="campaign-monitor"'.($currentMethod['id'] !== 'campaign-monitor'?' style="display:none;"':'').'>';
                                        echo '<p>You get your API key by logging into your <a href="https://www.campaignmonitor.com" target="_blank">campaignmonitor.com</a> account, going to <strong>Account Settings</strong>, then clicking <strong>API Keys</strong>.</p>';
                                        echo '<p>Make sure to click the “<em>Show API Key</em>” link to get your API Key, instead of your Client ID.</p>';
                                    echo '</div>';

                                    echo '<div data-methods="mailchimp"'.($currentMethod['id'] !== 'mailchimp'?' style="display:none;"':'').'>';
                                        echo '<p>You get your API key by logging into your <a href="https://www.mailchimp.com" target="_blank">MailChimp</a> account, going to <strong>Profile</strong>, going to <strong>Extras</strong> then clicking <strong>API Keys</strong>.</p>';
                                    echo '</div>';
                                }

                            echo '</td>';
                        echo '</tr>';

                    }

                    echo '</tbody>';
                echo '</table>';


                submit_button();
            echo '</form>';

        echo '</div>';
    }
}