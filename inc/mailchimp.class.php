<?php namespace Dorigo\GatedContent;

class MailChimp extends Base {
    protected function __construct($apikey = null) {
        parent::__construct($apikey);

        if($this->apiKey) {
            $this->api = new \Mailchimp($this->apiKey);
        }
    }

    public function lists() {
        try {
            $temp = $this->api->lists->getList([], 0, 100);
        } catch (\Exception $e) {
            $this->error($e);
        }

        $lists = [];

        foreach($temp['data'] as $list) {
            $lists[$list['id']] = $list['name'];
        }

        return $lists;
    }

    public function subscribe() {
        $list   = isset($_POST['list'])   ? $_POST['list']   : $this->list;
        $email  = isset($_POST['email'])  ? $_POST['email']  : false;

        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->showResult([
                'status' => 'error',
                'code' => 400,
                'message' => 'Please enter a valid email address.',
            ]);
        }

        try {
            $result = $this->api->lists->subscribe(
                $list,
                ['email' => $email]
            );
        } catch (\Mailchimp_Error $e) {
            $this->error($e);
        }

        $this->setCookie($_POST['content'], $email);

        $this->showResult([
            'message' => $this->successMessage,
        ]);
    }

    public function clients() {
        return [];
    }
}