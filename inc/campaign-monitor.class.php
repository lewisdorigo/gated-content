<?php namespace Dorigo\GatedContent;

class CampaignMonitor extends Base {
    private $auth;

    protected function __construct($apikey = null) {
        parent::__construct($apikey);

        if($this->apiKey) {
            $this->auth = ['api_key' => $this->apiKey];
        }
    }

    public function clients() {
        $wrap = new \CS_REST_General($this->auth);
        $result = $wrap->get_clients();

        if(!$result->was_successful()) {
            return [];
        }

        $clients = [];

        foreach($result->response as $client) {
            $clients[$client->ClientID] = $client->Name;
        }

        return $clients;
    }

    public function lists($client = null) {
        $client = $client ?: $this->client;

        $wrap = new \CS_REST_Clients($client, $this->auth);
        $result = $wrap->get_lists();

        if(!$result->was_successful()) {
            return [];
        }

        $lists = [];

        foreach($result->response as $client) {
            $lists[$client->ListID] = $client->Name;
        }

        return $lists;
    }

    public function subscribe() {
        $client = isset($_POST['client']) ? $_POST['client'] : $this->client;
        $list   = isset($_POST['list'])   ? $_POST['list']   : $this->list;
        $email  = isset($_POST['email'])  ? $_POST['email']  : false;

        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->showResult([
                'status' => 'error',
                'code' => 400,
                'message' => 'Please enter a valid email address.',
            ]);
        }

        $wrap = new \CS_REST_Subscribers($list, $this->auth);

        $result = $wrap->add([
            'EmailAddress' => $email,
            'Resubscribe' => true
        ]);

        if(!$result->was_successful()) {
            $this->error($result);
        }

        $this->setCookie($_POST['content'], $email);

        $this->showResult([
            'message' => $this->successMessage,
        ]);
    }
}