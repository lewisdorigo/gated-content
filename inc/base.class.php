<?php namespace Dorigo\GatedContent;

abstract class Base {
    private static $instance;

    private $defaultResult = [
        'status' => 'success',
        'code'   => 200,
        'message' => '',
    ];

    protected $apiKey = false;
    protected $list = null;
    protected $client = null;

    protected $api;
    protected $successMessage = 'success';

    protected $ajaxAction = 'drgo_gated_submit';

    protected $formFields;

    protected function __construct($apikey = null) {
        $this->apiKey = $apikey;

        $this->getOptions();
        $this->addFilters();

        $this->formFields = [
            '<input type="hidden" name="action" value="'.$this->ajaxAction.'">',
            '<input type="hidden" name="gated-form" value="1">',
        ];
    }

    public static function instance($apikey = null) {
        $objectkey = $apikey ?: 'default';

        if(!isset(self::$instance[$objectkey])) {
            $self = get_called_class();
            self::$instance[$objectkey] = new $self($apikey);
        }

        return self::$instance[$objectkey];
    }


    public static function formFields() {
        $instance = self::instance();

        $fields = $instance->formFields;

        if(!array_key_exists('list', $fields) && function_exists('get_sub_field') && get_sub_field('list')) {
            $fields['list'] = '<input type="hidden" name="list" value="'.get_sub_field('list').'">';
        }

        if(!array_key_exists('content', $fields) && (is_single() || is_singular() || is_page())) {
            $fields['content'] = '<input type="hidden" name="content" value="'.get_queried_object_id().'">';
        }

        return implode(PHP_EOL, $fields);
    }

    protected function setCookie($id, $value = true) {
        $cookieName = \Dorigo\GatedContent::$cookieName;

        $cookie = array_key_exists($cookieName, $_COOKIE) ? json_decode($_COOKIE[$cookieName], true) : [];
        $cookie[$id] = $value;

        setcookie($cookieName, json_encode($cookie), time()+(365*24*60*60), '/');
    }

    protected function getOptions() {
        if(!$this->apiKey) {
            if($key = get_option('drgo_gated_api_key')) {
                $this->apiKey = $key;
            } else {
                $this->apiKey = false;
                $this->error(new \Exception('There is no API Key'));
            }
        }

        $this->list = get_option('drgo_gated_list');
        $this->client = get_option('drgo_gated_client');
    }

    protected function addFilters() {
        add_action('wp_ajax_'.$this->ajaxAction, [$this,'subscribe']);
        add_action('wp_ajax_nopriv_'.$this->ajaxAction, [$this,'subscribe']);


        add_action('wp_ajax_drgo_gated_lists', [$this,'getLists']);
    }

    protected function error($error) {
        return $this->showResult([
            'status'  => 'error',
            'message' => $error->getMessage(),
            'code'    => $error->getCode()
        ], $error->getCode());
    }

    protected function showResult($object, $code = 200) {
        header('Content-Type: application/json');

        $code = $code < 200 || $code > 600 ? 500 : $code;

        http_response_code($code);

        print json_encode(array_merge($this->defaultResult, $object));
        die();
    }

    public function getLists() {
        $method = isset($_POST['method']) ? $_POST['method'] : false;
        $apiKey = isset($_POST['apiKey']) ? $_POST['apiKey'] : false;
        $client = isset($_POST['client']) ? $_POST['client'] : false;

        if(!$method || !$apiKey) {
            $this->error(new \Exception('A method and API Key are required'));
        }

        $method = trim($method);
        $apiKey = trim($apiKey);

        $methods = \Dorigo\GatedContent::instance()->getMethods();

        if(!array_key_exists($method, $methods)) {
            $this->error(new \Exception('The method `<code>'.$method.'</code>` is not available'));
        }

        $api = $methods[$method]['class']::instance($apiKey);

        $return = [
            'lists' => [],
            'clients' => [],
        ];

        if(method_exists($api, 'clients')) {
            $return['clients'] = $api->clients();
        }

        if(method_exists($api, 'lists')) {
            $return['lists'] = $api->lists($client);
        }

        return $this->showResult([
            'message' => $return,
        ]);
    }

    abstract public function lists();
    abstract public function clients();
    abstract public function subscribe();
}