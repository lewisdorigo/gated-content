;(function(w, $) {

$(document).ready(function() {

    $('input[name="gated-form"]').closest('form').on('submit', function(e) {
        e.preventDefault();

        var $this  = $(this),
            data   = $this.serialize(),
            action = this.getAttribute('action'),
            method = this.getAttribute('method');

        if(!$this.find('.form-loading')[0]) {
            $this.append('<div class="form-loading">');
        }

        $this.addClass('loading');

        $.ajax({
            url: action,
            data: data,
            method: method,
            complete: (function(response, status) {
                if(!$this.find('.form-notice')[0]) {
                    $this.append('<aside class="form-notice">');
                }

                if(typeof(response.responseJSON) === 'object') {
                    response = response.responseJSON;
                }

                status = response.status;

                var $notice = $this.find('.form-notice');
                $notice.html(response.message);

                if(status === 'error') {
                    $notice.removeClass('form-notice--success').addClass('form-notice--error');
                } else {
                    $notice.removeClass('form-notice--error').addClass('form-notice--success');
                }

                $this.removeClass('loading');
                $this.find('.form-loading').remove();

                if(status === 'success') {
                    window.location.reload();
                }
            }),
        });

        return false;
    });

});

})(window, jQuery);
