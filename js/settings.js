"use strict";

(function($) {
    window.apiKeys = {};
    var lastMethod = $('#drgo_gated_method').val();
    window.apiKeys[lastMethod] = $('#drgo_gated_api_key').val();

    $('body').on('change', '#drgo_gated_method', function(e) {
        var method = $(this).val();

        window.apiKeys[lastMethod] = $('#drgo_gated_api_key').val();

        if(typeof(window.apiKeys[method]) !== 'undefined') {
            $('#drgo_gated_api_key').val(window.apiKeys[method]).change();
        } else {
            $('#drgo_gated_api_key').val('').change();
        }

        $('[data-methods]').hide();
        $('[data-methods*="'+method+'"]').show();

        lastMethod = method;
    });

    $('#drgo_gated_api_key').on('change', function(e) {

        $('#drgo_gated_client, #drgo_gated_list').val('').find('option:not([disabled])').remove();

        getGatedLists();
    });

    $('#drgo_gated_client').on('change', function(e) {
        $('#drgo_gated_list').find('option:not([disabled])').remove();

        getGatedLists(true);
    });


    function getGatedLists(ignore) {
        var ignoreClients = ignore || false;

        var method = $('#drgo_gated_method').val();
        var apiKey = $('#drgo_gated_api_key').val();
        var client = $('#drgo_gated_client').val();

        if(!method || !apiKey) { return; }

        $('#gated-content-form').addClass('form-loading');
        $('#submit').attr('disabled');

        $.ajax({
            url: wp.ajax.settings.url,
            data: {
                'action': 'drgo_gated_lists',
                'method': method,
                'apiKey': apiKey,
                'client': client,
            },
            method: 'post',
            complete: (function(response, status) {
                if(typeof(response.responseJSON) === 'object') {
                    response = response.responseJSON;
                }

                status = response.status;

                if(status === 'error') {
                    alert(response.message);
                    return;
                }

                if(!ignoreClients) {
                    $.each(response.message.clients, function(id, name) {
                        $('#drgo_gated_client').append('<option value="'+id+'">'+name+'</option>');
                    });
                }

                $.each(response.message.lists, function(id, name) {
                    $('#drgo_gated_list').append('<option value="'+id+'">'+name+'</option>');
                });

                $('#gated-content-form').removeClass('form-loading');
                $('#submit').removeAttr('disabled');
            }),
        });
    }

})(jQuery);