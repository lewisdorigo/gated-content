"use strict";

(function($, postL10n, postGated) {

    var postL10n = postL10n || {};
    postL10n['gated'] = 'Gated Content';

    $('label[for="visibility-radio-private"]').after('<br>'+
        '<input type="radio" name="visibility" id="visibility-radio-gated" value="gated"> '+
        '<label for="visibility-radio-gated" class="selectit">Gated Content</label>');


    if(postGated.isGated === '1') {
        $('input[name="visibility"]').removeProp('checked').filter('[value="gated"]').prop('checked',true).change();
        $('#post-visibility-display').html(postL10n.gated);
    }
})(jQuery, postL10n, postGated);