"use strict";

(function($, acf) {
    $('body').on('click', '.save-post-visibility',  function(e) {
        var visibility = jQuery('input[name="visibility"]:checked').val();

        acf.ajax.update('post_visibility', visibility).fetch();
    });
})(jQuery, acf);