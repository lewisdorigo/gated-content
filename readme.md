# Gated Content for Wordpress

Allows certain content to be set to Gated in WordPress, requiring the user to
sign up for a mailing list (either MailChimp or Campaign Monitor) before viewing
the content.

Requires Advanced Custom Fields PRO and Composer